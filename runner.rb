#!/usr/bin/env ruby

require_relative "Simon"
require 'optparse'
require 'rubygems'
require 'bundler/setup'


options = {}

opt_parser = OptionParser.new do |opt|
	opt.banner = "Usage: runner [OPTIONS]"
	opt.separator  ""
	opt.separator  "Options"

	opt.on("-o","--outputdir OUTPUTDIR","the output directory for the reworked files") do |outputdir|
		options[:outputdir] = outputdir
	end

	opt.on("-s","--svadir SVADIR","the subsonic various artist output directory for the linked files") do |svadir|
		options[:svadir] = svadir
	end

	opt.on("-i","--inputdir INPUTDIR","the input directory which contains the MP3s") do |inputdir|
		options[:inputdir] = inputdir
	end

	opt.on("-d","--delete","delete the input files?") do
		options[:delete] = true
	end

	opt.on("-t","--trial","process the files without making changes? (useful when isolating errors)") do
		options[:trial] = true
	end

	opt.on("-h","--help","help") do
		puts opt_parser
		exit
	end
end

opt_parser.parse!

if (options.length < 2) then
	puts "Please run this with the --help option"
	exit
end

if (options[:inputdir].nil?) then
	puts "Please provide an input directory"
	exit
end

if (options[:outputdir].nil?) then
	puts "Please provide an output directory"
	exit
end

simon = Simon.new(options)
simon.process

