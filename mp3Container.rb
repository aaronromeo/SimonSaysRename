class Mp3Container
	attr_accessor :filename
	attr_accessor :artist
	attr_accessor :albumArtist
	attr_accessor :title
	attr_accessor :album
	attr_accessor :tracknumber
	attr_accessor :discnumber
	attr_accessor :ismodified
	attr_accessor :iscompilation
	attr_accessor :mp3AlbumArtistField
	attr_accessor :mp3ArtistField
	attr_accessor :isArtworkInstalled
		
	def initialize(filename)
		@filename = filename
		@album = nil
		@artist = nil
		@albumArtist = nil
		@title = nil
		@tracknumber = nil
		@discnumber = nil
		@ismodified = false
		@iscompilation = false
		@mp3AlbumArtistField = nil
		@mp3ArtistField = nil
		@isArtworkInstalled = false
	end
	
	def extract(mp3)
		tag = mp3.id3v2_tag
		
		if (!tag.frame_list('TALB').nil? && tag.frame_list('TALB').first.to_s!='') then
			@album = tag.frame_list('TALB').first.to_s
		elsif (!tag.frame_list('TAL').nil? && tag.frame_list('TAL').first.to_s!='') then
			@album = tag.frame_list('TAL').first.to_s
		else
			raise throwErrorMessage(tag, 'Album')
		end
		
		@discnumber = '00'
		if (!tag.frame_list('TPOS').nil? && tag.frame_list('TPOS').first.to_s!='') then
			@discnumber = (tag.frame_list('TPOS').first.to_s.split('/')[0]).rjust(2, '0')
		elsif (!tag.frame_list('disc_number').nil? && tag.frame_list('disc_number').first.to_s!='') then
			@discnumber = tag.frame_list('disc_number').first.to_s.rjust(2, '0')
		elsif (!tag.frame_list('TPA').nil? && tag.frame_list('TPA').first.to_s!='') then
			@discnumber = (tag.frame_list('TPA').first.to_s.split('/')[0]).rjust(2, '0')
		end

		@tracknumber="00"
		if (!tag.frame_list('TRCK').first.nil? && tag.frame_list('TRCK').first.to_s!='') then
			rawtracknumber = tag.frame_list('TRCK').first.to_s.split('/')[0]
			if (!rawtracknumber.nil?) then
				@tracknumber = rawtracknumber.rjust(2, '0')
			end
		elsif (!tag.frame_list('TRK').nil? && tag.frame_list('TRK').first.to_s!='') then
			rawtracknumber = tag.frame_list('TRK').first.to_s.split('/')[0]
			if (!rawtracknumber.nil?) then
				@tracknumber = rawtracknumber.rjust(2, '0')
			end
		#else
			#raise throwErrorMessage(mp3.tag2, 'Track Number')
		end

		@mp3ArtistField = ''
		if (!tag.frame_list('TPE1').nil? && tag.frame_list('TPE1').first.to_s!='') then
			@mp3ArtistField = 'TPE1'
		elsif (!tag.frame_list('TP1').nil? && tag.frame_list('TP1').first.to_s!='') then
			@mp3ArtistField = 'TP1'
		else
			raise throwErrorMessage(mp3.tag2, 'Artist')
		end
		tempArtist = tag.frame_list(@mp3ArtistField).first.to_s
		@artist = santitizeArtist(tempArtist)
		if (@artist != tempArtist) then
			@ismodified = true
		end

		@mp3AlbumArtistField = nil
		if (!tag.frame_list('TPE2').nil? && tag.frame_list('TPE2').first.to_s!='') then
			@mp3AlbumArtistField = 'TPE2'
		elsif (!tag.frame_list('TP2').nil? && tag.frame_list('TP2').first.to_s!='') then
			@mp3AlbumArtistField = 'TP2'
		end
		if (!@mp3AlbumArtistField.nil?) then
			tempArtist = tag.frame_list(@mp3AlbumArtistField).first.to_s
			@albumArtist = santitizeArtist(tempArtist)
			if (@albumArtist != tempArtist) then
				@ismodified = true
			end
		else
			@mp3AlbumArtistField = 'TPE2'
			@albumArtist = @artist
			@ismodified = true
		end

		@iscompilation = false
		if ( !tag.frame_list('TCMP').nil? && tag.frame_list('TCMP').first.to_s=='1' ) then
			@iscompilation = true
		end
		
		if (!tag.frame_list('TIT2').nil? && tag.frame_list('TIT2').first.to_s!='') then
			@title = tag.frame_list('TIT2').first.to_s
		elsif (!tag.frame_list('TT2').nil? && tag.frame_list('TT2').first.to_s!='') then
			@title = tag.frame_list('TT2').first.to_s
		else
			raise throwErrorMessage(mp3.tag2, 'Title')
		end
		
		if (!tag.frame_list('APIC').nil? || !tag.frame_list('PIC').nil?) then
			#text_encoding, mime_type, picture_type, picture_description, picture_data = mp3.tag2['APIC'].unpack('c Z* c Z* a*')
			@isArtworkInstalled = true
		end
		
	end
	
	private
	def throwErrorMessage(id3tag, field)
#		keys = Array.new
#		id3tag.each_key do |k|
		#TODO: Is this the correct way to add to an array?
#			keys.push(k)
#		end
#		return "Unable to find the " + field + " in the fields: " + keys.join(' ')
		return "Unable to find the " + field
	end

	private
	def santitizeArtist(curArtist)
		if (curArtist.end_with?(", The")) then
			curArtist = "The " + curArtist.gsub(', The', '')
		end
		curArtist.gsub!(/^[a-z]|\s+[a-z]/) { |a| a.upcase }
		return curArtist.scan(/[[:print:]]/).join.strip
	end
	
end