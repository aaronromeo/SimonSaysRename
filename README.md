# MP3 Tagger Reorganizer

This is a small program I have writen to help me organize my MP3s into a file structure which can be used with SubSonic.

It will result in your MP3s being repopulated into the form

    |- AlbumArtist
     |- Album
      |- Disc#-Track#-Artist-Title

This uses the [taglib](http://robinst.github.com/taglib-ruby) (gem install taglib-ruby). It requires the gem taglib-ruby, but check out the install instructions listed in the URL.

    $ ./runner.rb --help
    Usage: runner [OPTIONS]

    Options
        -o, --outputdir OUTPUTDIR        the output directory for the reworked files
        -s, --svadir SVADIR              the subsonic various artist output directory for the linked files
        -i, --inputdir INPUTDIR          the input directory which contains the MP3s
        -d, --delete                     delete the input files?
        -t, --trial                      process the files without making changes? (useful when isolating errors)
        -h, --help                       help
