### read and display infos & tags
require "rubygems"
#require 'ruby-debug'
require 'fileutils'
require 'taglib'
require_relative "mp3Container"

# read and display infos & tags
#Mp3Info.open("myfile.mp3") do |mp3info|
#  puts mp3info
#end

# read/write tag1 and tag2 with Mp3Info#tag attribute
# when reading tag2 have priority over tag1
# when writing, each tag is written.
#Mp3Info.open("myfile.mp3") do |mp3|
#  puts mp3.tag.title
#  puts mp3.tag.artist
#  puts mp3.tag.album
#  puts mp3.tag.tracknum
#  mp3.tag.title = "track title"
#  mp3.tag.artist = "artist name"
#end
# tags are written when calling Mp3Info#close or at the end of the #open block
### access id3v2 tags
class Simon
	def initialize(options)
		@options = options
	end

	def process()
		inputfiles = Dir.glob(File.join(@options[:inputdir], "**", "*.mp3"))
		inputdirs = Hash.new
		
		inputfiles.each do |inputfile|
			resultingdir = File.dirname(inputfile)
			
			filesInDir = Array.new
			if (inputdirs.has_key?(resultingdir)) then
				filesInDir = inputdirs[resultingdir]
			end
			
			filesInDir.push(inputfile)
			inputdirs[resultingdir] = filesInDir
		end

		@destDirPath = @options[:outputdir]

		@destRootDir = Dir.new(@destDirPath)
		inputdirs.each do |dirname, files|
			@sourceRootDir = Dir.new(dirname)
			errState = dirProcess(files, @options[:svadir])
			if (!errState && Dir.entries(dirname) == [".", ".."]) then
				Dir.rmdir(dirname)
			end
		end

	end

	protected
	def buildDirectoryPath(mp3data, basePath, isCompliation=false)
		pathArray = Array.new
		if (isCompliation) then
			pathArray.push(mp3data.artist)
		else
			pathArray.push(mp3data.albumArtist)
		end
		pathArray.push(mp3data.album)

		newPath = basePath
		pathArray.each do |newPathNode|
			newPath = File.join(newPath, newPathNode.gsub(/[\/?*:;{}"'.\\]/, '_') )
			if (!File.directory?(newPath)) then
				Dir.mkdir(newPath)
				@createdAndCopiedFiles.push(newPath)
			end
		end

		return newPath
	end

	protected
	def getFileName(mp3data)
		filenameArray = Array.new
		filename = ''

		filenameArray.push( mp3data.discnumber )
		filenameArray.push( mp3data.tracknumber )
		filenameArray.push( mp3data.artist )
		filenameArray.push( mp3data.title )

		filenameArray.each do |part|
			if (filename.length > 0) then
				filename << '-'
			end
			filename << part.gsub(/[\/?*:;{}"'.\\]/, '_')
		end
		filename << ".mp3"

		return filename
	end

	protected
	def dirProcess(mp3FileNames, subsonicVADir)

		@createdAndCopiedFiles = Array.new
		isErrorState = false
		puts("Processing: " << @sourceRootDir.path)
		fe = File.new("error.txt", "a")

		if (!File.directory?(@destDirPath)) then
			Dir.mkdir(@destDirPath)
			@createdAndCopiedFiles.push(@destDirPath)
		end

		if (!subsonicVADir.nil? && !File.directory?(subsonicVADir)) then
			Dir.mkdir(subsonicVADir)
			@createdAndCopiedFiles.push(subsonicVADir)
		end
		
		images = Array.new
		if (mp3FileNames.length == 0) then
			fe.puts("Dir Error: " + @sourceRootDir.path)
			isErrorState = true
		end

		#isDifferent = false
		mp3Data = Array.new
		isAlbumArtistSet = true
		isCompilation = false
		artistsInAlbum = Hash.new()
		lastArtist = nil
		mp3FileNames.each do |mp3FileName|
			# Validate all MP3s
			
			container = Mp3Container.new(mp3FileName)
			begin
				TagLib::MPEG::File.open(mp3FileName) do |mp3|
					container.extract(mp3)
				end
			rescue => detail
				fe.puts("File Error: " + mp3FileName)
				fe.puts(detail.message)
				fe.puts(detail.backtrace)
				isErrorState = true
			end
			if (container.nil?) then
				isErrorState = true
				#Don't break here because we can flag all the files which have a problem
			else
				if (container.albumArtist.nil?) then
					isAlbumArtistSet = false
				end

				if (container.iscompilation) then
					isCompilation = true
				end

				artistsInAlbum[container.albumArtist] = 1
			
				mp3Data.push(container)
			end
		end #first loop

		if (isErrorState || artistsInAlbum.keys.length > 1 || (!isAlbumArtistSet && !isCompilation) ) then
			fe.puts("Dir Error: " + @sourceRootDir.path)
			isErrorState = true
		else
			mp3Data.each do |mp3container|

				if ( isCompilation && artistsInAlbum.keys.length > 1 && 
									mp3container.albumArtist != "Various Artists") then
					mp3container.ismodified = true
					mp3container.albumArtist = "Various Artists"
				end
				
				begin
					newDir = buildDirectoryPath(mp3container, @destDirPath)
					if (isCompilation && subsonicVADir) then
						svaDir = buildDirectoryPath(mp3container, subsonicVADir, isCompilation)
					else
						svaDir = ''
					end
					filename = getFileName(mp3container)

					if (!newDir.nil? && !filename.nil?) then
						absfilename = File.join(newDir, filename)
						puts("Processing: " << mp3container.filename)
						images = Dir.glob(File.join(@sourceRootDir.path, "*.jpg")) + Dir.glob(File.join(@sourceRootDir.path, "*.jepg")) + Dir.glob(File.join(@sourceRootDir.path, "*.gif")) + Dir.glob(File.join(@sourceRootDir.path, "*.png"))
						images.each do |image|
							newFileName = File.join(newDir, File.basename(image))
							if (!File.file?(newFileName)) then
								puts("Mapping: " << image << " TO " << newFileName)
								if (!@options[:trial]) then
									FileUtils.cp(image, newFileName)
									@createdAndCopiedFiles.push(newFileName)
								end
							end
						end
						if (File.file?(absfilename)) then
							fe.puts("File Error: " + mp3container.filename)
							fe.puts "\tDuplicate file: " + mp3container.filename
							absfilename << ".duplicate"
							isErrorState = true
						end
						puts("Mapping: " << mp3container.filename << " TO " << absfilename)
						FileUtils.cp(mp3container.filename, absfilename)
						@createdAndCopiedFiles.push(absfilename)
						
						if (isCompilation && !svaDir.nil?) then
							puts("Linking: " << absfilename << " TO " << File.join(svaDir, filename))
							FileUtils.ln_s(absfilename, File.join(svaDir, filename))
						end

						if (mp3container.ismodified) then
							TagLib::MPEG::File.open(absfilename) do |mp3|
								puts("Modifying: Artist for " << absfilename)
								tag = mp3.id3v2_tag
								albumArtistFrame = tag.frame_list(mp3container.mp3AlbumArtistField).first
								if (!albumArtistFrame.nil?) then
									albumArtistFrame.text = mp3container.albumArtist
								else
									albumArtistFrame = TagLib::ID3v2::TextIdentificationFrame.new(mp3container.mp3AlbumArtistField, TagLib::String::Latin1)
									albumArtistFrame.text = mp3container.albumArtist
									tag.add_frame(albumArtistFrame)
								end
								artistFrame = tag.frame_list(mp3container.mp3ArtistField).first
								artistFrame.text = mp3container.artist
								
								mp3.save
							end
						end
						if (!mp3container.isArtworkInstalled) then
							covers = Dir.glob(File.join(newDir, "cover.jpg")) + Dir.glob(File.join(newDir, "folder.jpg"))
							covers.each do |cover|
								TagLib::MPEG::File.open(absfilename) do |mp3|
									tag = mp3.id3v2_tag
									puts("Modifying: Image for " << absfilename)
									apic = TagLib::ID3v2::AttachedPictureFrame.new
									apic.mime_type = "image/jpeg"
									apic.description = "Cover"
									apic.type = TagLib::ID3v2::AttachedPictureFrame::FrontCover
									apic.picture = File.open(cover, 'rb') { |f| f.read }
									tag.add_frame(apic)

									mp3.save
								end
							end
						end
					end
				rescue => detail
					fe.puts("File Error: " + mp3container.filename)
					fe.puts(detail.message)
					fe.puts(detail.backtrace)
					isErrorState = true
				end
			end
		end
		fe.close
		if (!File.size?(fe.path)) then
			File.delete(fe.path)
		end
		if (!isErrorState && @options[:delete] && !@options[:trial]) then
			mp3FileNames.each do |file|
				FileUtils.rm(file)
			end
			images.each do |file|
				FileUtils.rm(file)
			end
		end
		if (@options[:trial]) then
				@createdAndCopiedFiles.reverse.each do |file|
				FileUtils.rm_r(file)
			end
		end
		
		return isErrorState
	end
end
